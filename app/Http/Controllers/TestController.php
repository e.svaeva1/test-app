<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class TestController extends Controller
{
    const RESULTS = [
        'head',
        '$',
        'post, get',
        '10',
        'система управления базами данных',
        '6',
    ];

    public function showTest(Request $request) {
        return view('test');
    }
    
    public function processTest(Request $request) {
        $validateFields = [];
        foreach (self::RESULTS as $key => $value) {
            $validateFields[$key] = 'required';
        }
        $data = $this->validate($request, $validateFields);
        
        return back()->with('success', 'Результат: ' . $this->correctResultsAmt($data));
    }

    public function calcResults(array $array) {
        $count = 0;
        $wrongAnswers = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = implode(', ', $value);
            }
            if (strtolower($value) == self::RESULTS[$key]) {
                $count++;
            } else {
                $newKey = (int)$key + 1;
                $wrongAnswers[] = "$newKey. Правильный ответ: " . self::RESULTS[$key];
            }
        }
        return [ $count, $wrongAnswers ];
    }

    public function correctResultsAmt(array $data) {
        [ $count, $wrongAnswers ] = $this->calcResults($data);

        $resultStr = $count . " / " . count(self::RESULTS) . "\n";

        foreach ($wrongAnswers as $answer) {
            $resultStr = $resultStr . "\n" . $answer;
        }

        return $resultStr;
    }
}