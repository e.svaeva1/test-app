<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Тест</title>

        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <style>
            body {
                font-family: 'Nunito', sans-serif;
                padding: 1%;
                width: 50%;
                min-width: 600px;
                display: block;
                margin-left: auto;
                margin-right: auto;
                margin-top: 1%;
                word-break: break-word;
            }
            .list {
                padding: 2%;
            }
            .layout {
                display: grid;
                grid-template-columns: 33% 33% 33%;
            }
            .layout-element {
                padding: 5px;
            }
            .centered {
                text-align: center;
                padding: 10px;
                margin: 0;
            }
            button:hover {
                background-color: #4bd4d4;
                color: white;
            }
            button {
                background-color: white;
                color: black;
                border: 2px solid #4bd4d4;
                padding: 15px 30px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 0;
                margin-right: 5px;
                margin-left: 5px;
                border-radius: 5px;
            }
            .title {
                font-weight: bold;
                font-size: xx-large;
            }
            .invalid-input-message {
                color: red;
                font-weight: bold;
            }
            input[type='text'] {
                margin-top: 4px;
                padding: 4px 8px;
                width: 75%;
            }
            .question {
                font-style: italic;
            }
            .invalid-input-border {
                border: 2px solid red;
            }
            .code {
                color: white;
                background-color: lightseagreen;
                font-weight: bold;
                padding-right: 3px;
                padding-left: 3px;
                border-radius: 5px;
            }
            input[type='radio'] {
                width: 14px;
                height: 14px;
                border-radius: 50%;
                outline: none;
            }
            select {
                width: 75%;
                margin-top: 4px;
                padding: 4px 8px;
            }
            textarea {
                width: 75%;
                margin-top: 4px;
                padding: 4px 8px;
            }
            .result-input-message {
                color: lightseagreen;
                font-weight: bold;
            }
            button:active {
                box-shadow: inset;
                transform: translateY(4px);
            }
        </style>
    </head>
    <body>
        <div class="list">
            <p class="centered title">Тест</p>
            <form action="{{ route('processTest') }}" method="post">
                @csrf
                <div class="layout centered">
                    <div class="layout-element">
                        <label class="question">Вопрос 1:<br></label>
                        <label>Какой тэг определяет заголовок документа HTML?<br></label>

                        <input type="text" name="0" id="0"><br/>

                        @error('0')
                            <label class="invalid-input-message" role="alert">Поле обязательно</label>
                        @enderror
                    </div>
                    <div class="layout-element">
                        <label class="question">Вопрос 2:<br></label>
                        <label>С какого символа начинаются имена переменных в PHP?<br></label>

                        <input type="radio" id="q1choice1" name="1" value="!">
                        <label for="q1choice1">!</label>

                        <input type="radio" id="q1choice2" name="1" value="#">
                        <label for="q1choice2">#</label>

                        <input type="radio" id="q1choice3" name="1" value="$">
                        <label for="q1choice3">$</label>

                        <input type="radio" id="q1choice4" name="1" value="%">
                        <label for="q1choice4">%</label>
                        <br/>
                        
                        @error('1')
                            <label class="invalid-input-message" role="alert">Поле обязательно</label>
                        @enderror
                    </div>
                    <div class="layout-element">
                        <label class="question">Вопрос 3:<br></label>
                        <label>Какие методы можно применять для отправки формы?<br></label>

                        <input type="checkbox" id="q2choice1" name="2[]" value="POST">
                        <label for="q2choice1">POST</label>
                        <input type="checkbox" id="q2choice2" name="2[]" value="TRY">
                        <label for="q2choice2">TRY</label>
                        <input type="checkbox" id="q2choice3" name="2[]" value="HEAD">
                        <label for="q2choice3">HEAD</label>
                        <input type="checkbox" id="q2choice4" name="2[]" value="GET">
                        <label for="q2choice4">GET</label>
                        <br/>
                        
                        @error('2')
                            <label class="invalid-input-message" role="alert">Поле обязательно</label>
                        @enderror
                    </div>
                    <div class="layout-element">
                        <label class="question">Вопрос 4:<br></label>
                        <label>Сколько байт займет строка "Привет", записанная в поле с типом
                            <span class="code">VARCHAR(10)</span>?<br></label>

                            <select name="3" id="3">
                                <option disabled selected hidden value></option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                            </select>
                            <br/>
                        
                        @error('3')
                            <label class="invalid-input-message" role="alert">Поле обязательно</label>
                        @enderror
                    </div>
                    
                    <div class="layout-element">
                        <label class="question">Вопрос 5:<br></label>
                        <label>Как расшифровывается аббревиатура СУБД?<br></label>

                        <textarea rows="2" name="4"></textarea>
                        <br/>
                        
                        @error('4')
                            <label class="invalid-input-message" role="alert">Поле обязательно</label>
                        @enderror
                    </div>
                    <div class="layout-element">
                        <label class="question">Вопрос 6:<br></label>
                        <label>Что будет в переменной $result после выполнения кода 
                            <span class="code">$i = 5; $result = ++i;</span>?<br></label>

                        <input type="radio" id="q5choice1" name="5" value="7">
                        <label for="q5choice1">7</label>

                        <input type="radio" id="q5choice2" name="5" value="5">
                        <label for="q5choice2">5</label>

                        <input type="radio" id="q5choice3" name="5" value="6">
                        <label for="q5choice3">6</label>

                        <input type="radio" id="q5choice4" name="5" value="4">
                        <label for="q5choice4">4</label>
                        <br/>
                        
                        @error('5')
                            <label class="invalid-input-message" role="alert">Поле обязательно</label>
                        @enderror
                    </div>
                </div>
                <div class="centered">
                    <button type="submit" value="submit">Отправить</button>
                </div>
            </form>
            <p class="centered result-input-message" style="white-space: pre-line">
                @if(Session::has('success'))
                    {{Session::get('success')}}
                @endif
            </p>
        </div>
    </body>
</html>
